import Foundation

struct Currency {
    var code: String
    var symbol: String
    var id: Int
    var name: String
}
extension Currency {
    init?(with json: [String: Any]) {
        guard let id = json["currency_id"] as? Int else {
            return nil
        }
        guard let code = json["code"] as? String else {
            return nil
        }
        guard let symbol = json["symbol"] as? String else {
            return nil
        }
        guard let name = json["name"] as? String else {
            return nil
        }
        self.id = id
        self.name = name
        self.symbol = symbol
        self.code = code
    }
    
    static func customInit(id: Int, name: String, code: String, symbol: String) -> Currency {
        let currency = Currency(code: code, symbol: symbol, id: id, name: name)
        return currency
    }
}

extension Currency {
    
    static func getList(callback: @escaping (([Currency]) -> Void)) {
        
        guard  let url = URL(string: "http://52.66.23.190:3020/get_all_currency") else {
            return
        }
        
        let urlRequest = URLRequest(url: url)
        ApiManager.postApi(withURLRequest: urlRequest) { (error, json) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard let json = json else {
                return
            }
            var currencies = [Currency]()
            guard let currenciesjson = json["data"] as? [[String: Any]] else {
                return
            }
            for currency in currenciesjson {
                if let currencyObject = Currency(with: currency) {
                    currencies.append(currencyObject)
                }
            }
            callback(currencies)
        }
    }
}
