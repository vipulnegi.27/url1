




import Foundation
class ApiManager {
    static func postApi(withURLRequest urlRequest: URLRequest, callback: @escaping ((Error?, [String: Any]?) -> Void)) {
        var request = urlRequest
        request.httpMethod = "POST"
        let urlSession = URLSession(configuration: .default)
        let dataTask = urlSession.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                callback(error, nil)
                return
            }
            guard let data = data else {
                return
            }
            let json = JsonParser.serializeData(data: data)
            callback(nil, json)
            
        }
        dataTask.resume()
    }
}

