import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "DisplayListTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "DisplayListTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        getCurrency()
    }
    var currencyList = [Currency]()
    
    func getCurrency () {
        Currency.getList { (currencies) in
            print(currencies)
            DispatchQueue.main.async {
                self.currencyList = currencies
                self.tableView.reloadData()
            }
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currencyList.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DisplayListTableViewCell") as?  DisplayListTableViewCell else {
            fatalError("Failed to initialize")
        }
        let data = currencyList[indexPath.row]
        cell.currencyId.text = "ID: \(data.id)"
        cell.currencyCode.text = "Code: \(data.code)"
        cell.currencySymbol.text = "Symbol: \(data.symbol)"
        cell.currencyName.text = "Name: \(data.name)"
        return cell
    }
}
